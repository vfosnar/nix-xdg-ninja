{
  description =
    "Automatically extracted xdg-ninja variables for use with home-manager";

  inputs = { };

  outputs = { self, ... }: {
    homeManagerModules.default = import ./output.nix;
  };
}
