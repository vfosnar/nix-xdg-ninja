import json
from pathlib import Path
import re
import subprocess

BROKEN_VARIABLES = ["GTK_RC_FILES", "GTK2_RC_FILES", "HISTFILE", "RXVT_SOCKET", "XAUTHORITY", "ZDOTDIR"]

def parse_file(text):
    if "currently unsupported" in text.lower() or "supported by default" in text.lower() or "alias" in text.lower():
        return

    matches = re.findall(r"\nexport ([^=]+)=\"\$XDG_([^\"]+)\"\/(.+)", text)
    for match in matches:
        yield match

def git_clone(repo: str, destination: Path):
    subprocess.check_call(["git", "clone", "--depth", "1", repo, destination])

def git_pull(directory: Path):
    subprocess.check_call(["git", "pull"], cwd=directory)

def extract_env_variables(xdg_ninja_path: Path):
    env_variables = {}
    for file_path in xdg_ninja_path.glob("./programs/*.json"):
        with open(file_path, "r", encoding="utf-8") as file:
            data = json.load(file)
            for file in data["files"]:
                for entry in parse_file(file["help"]):
                    if entry[0] not in env_variables.keys():
                        env_variables[entry[0]] = (entry[1], entry[2].strip())

    for broken_variable_name in BROKEN_VARIABLES:
        env_variables.pop(broken_variable_name)

    return env_variables

def main():
    xdg_ninja_path = Path("xdg-ninja")
    if xdg_ninja_path.exists():
        git_pull(xdg_ninja_path)
    else:
        git_clone("https://github.com/b3nj5m1n/xdg-ninja.git", xdg_ninja_path)

    env_variables = extract_env_variables(xdg_ninja_path)

    XDG_DIR_TO_VAR_MAP = {
        "CACHE_HOME": "cacheHome",
        "CONFIG_HOME": "configHome",
        "DATA_HOME": "dataHome",
        "STATE_HOME": "stateHome",
    }

    output = """{ config, lib, ... }:

{
  options = {
    xdg.cacheHome = lib.mkOption { type = lib.types.path; };
    xdg.configHome = lib.mkOption { type = lib.types.path; };
    xdg.dataHome = lib.mkOption { type = lib.types.path; };
    xdg.stateHome = lib.mkOption { type = lib.types.path; };
  };
  config.home.sessionVariables = {
    XDG_CACHE_HOME = config.xdg.cacheHome;
    XDG_CONFIG_HOME = config.xdg.configHome;
    XDG_DATA_HOME = config.xdg.dataHome;
    XDG_STATE_HOME = config.xdg.stateHome;
"""

    for name, (xdg_dir, path) in env_variables.items():
        if xdg_dir not in XDG_DIR_TO_VAR_MAP:
            continue
        var_name = XDG_DIR_TO_VAR_MAP[xdg_dir]

        output += f"""
    {name} = "${{config.xdg.{var_name}}}/{path}";"""

    output += """
  };
}
"""

    with open("output.nix", "w", encoding="utf-8") as file:
        file.write(output)

if __name__ == "__main__":
    main()
